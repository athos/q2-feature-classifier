q2-feature-classifier (2024.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Generate debian/control automatically to refresh version number
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Regenerate debian/control from debian/control.in (routine-update)
  * Autopkgtest for all supported Python3 versions
  * Regenerate debian/control from debian/control.in (routine-update)
  * Delaying true testing to autopkgtests since QIIME2 module cannot be
    registered at build time

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2024 15:45:11 +0100

q2-feature-classifier (2023.7.0-2) UNRELEASED; urgency=medium

  * d/copyright: update years and debian packagers.

 -- Étienne Mollier <emollier@debian.org>  Fri, 18 Aug 2023 15:23:36 +0200

q2-feature-classifier (2023.7.0-1) unstable; urgency=medium

  * New upstream version

 -- Étienne Mollier <emollier@debian.org>  Fri, 18 Aug 2023 14:34:12 +0200

q2-feature-classifier (2022.11.1-2) unstable; urgency=medium

  * sklearn-1.2.1.patch: add; fix most test failures with sklearn 1.2.1.

 -- Étienne Mollier <emollier@debian.org>  Thu, 02 Feb 2023 00:31:36 +0100

q2-feature-classifier (2022.11.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Bump versioned Depends
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 Jan 2023 11:06:08 +0100

q2-feature-classifier (2022.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2022.8.0
  * drop vsearch patch
  * Update dependency on qiime/q2-* >= 2022.8.0

 -- Mohammed Bilal <mdbilal@disroot.org>  Wed, 07 Sep 2022 11:22:16 +0000

q2-feature-classifier (2022.2.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * d/rules clean - removing egg-info files

 -- Steffen Moeller <moeller@debian.org>  Mon, 11 Jul 2022 01:28:39 +0200

q2-feature-classifier (2021.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Versioned (Build-)Depends qiime and q2 modules (>= 2021.8.0)

 -- Andreas Tille <tille@debian.org>  Fri, 01 Oct 2021 12:45:36 +0200

q2-feature-classifier (2020.11.1-3) UNRELEASED; urgency=medium

  * d/control: update uploader address
  * d/watch: fix broken url to github
  * d/salsa-ci.yml: disable build test on i386; missing build dependency
    python3-skbio on i386

 -- Étienne Mollier <emollier@debian.org>  Mon, 05 Jul 2021 23:07:21 +0200

q2-feature-classifier (2020.11.1-2) unstable; urgency=medium

  * vsearch.patch forwarded upstream.
  * Mark autopkgtest skip-not-installable.

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Sat, 06 Feb 2021 13:50:20 +0100

q2-feature-classifier (2020.11.1-1) unstable; urgency=medium

  [ Liubov Chuprikova ]
  * New upstream version
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Add new (Build-)Depends

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Build-Depends: q2-taxa
    Closes: #974839

  [ Étienne Mollier ]
  * Add build directory to PYTHONPATH for dh_auto_test.
  * Add vsearch.patch to fix --search_exact invalid options.
  * Update versioned dependencies and dropped redundant ones.
  * Add myself to uploaders.

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Thu, 28 Jan 2021 20:24:05 +0100

q2-feature-classifier (2019.4.0-1) unstable; urgency=medium

  * Initial release (Closes: #931887)

 -- Liubov Chuprikova <chuprikovalv@gmail.com>  Wed, 03 Jul 2019 19:59:31 +0200
